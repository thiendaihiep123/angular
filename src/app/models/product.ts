import { Customer } from "./customer";

export class Product {
    id : number = 0;
    cat! : Customer;
    name : String = "";
    description : String = "";
    image : String = "";
    create_at : Date = new Date();
    price : number = 0;
    sale_price : number = 0;
    status: boolean = false;
}
