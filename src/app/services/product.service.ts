import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient ) { }
  getProduct(): Observable<Product[]>{
    return this.http.get<Product[]>('/api/product');
  }

  insert(data: any): Observable<any>{
    return this.http.post<any>("/api/product/add", data);
  }

  detail(id:number): Observable<any>{
    return this.http.get<any>("/api/product/detail/" + id);
  }

  delete(id:number): Observable<any>{
    return this.http.get<any>("/api/product/delete/" + id);
  }

  update(data: any): Observable<any>{
    return this.http.post<any>("/api/product/update", data);
  }

  search(key: string): Observable<any>{
    return this.http.get<any>("/api/product/search?search=" + key);
  }

}
