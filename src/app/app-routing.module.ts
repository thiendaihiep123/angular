import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailComponent } from './components/detail/detail.component';
import { HomeComponent } from './components/home/home.component';
import { ProdaddComponent } from './components/prodadd/prodadd.component';
import { ProddetailComponent } from './components/proddetail/proddetail.component';
import { ProdeditComponent } from './components/prodedit/prodedit.component';
import { ShopComponent } from './components/shop/shop.component';


const routes: Routes = [
  {path : "" ,component:HomeComponent},
  {path : "shop" ,component:ShopComponent},
  {path : "detail" ,component:DetailComponent},
  {path : "prodadd" ,component:ProdaddComponent},
  {path : "proddetail/:id" ,component:ProddetailComponent},
  {path : "prodedit/:id" ,component:ProdeditComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
