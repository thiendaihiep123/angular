import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Msg } from 'src/app/models/msg';
import { Product } from 'src/app/models/product';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';
import { Customer } from '../../models/customer';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  id : number = 0;
  list: Customer[] = [];
  listb: Product[] = [];
  constructor(private sv: CustomerService,private svb: ProductService) { }
  msg : Msg = new Msg();
  
  searchForm:FormGroup = new FormGroup({
    key: new FormControl(),
  });

  ngOnInit(): void {
    this.sv.getCustomer().subscribe(res =>{
        this.list = res;
    })
    this.svb.getProduct().subscribe(res =>{
      console.log(res);
      this.listb = res;
      
    })

  }

  onSearch(){
      // alert(this.searchForm.value.key);
      this.svb.search(this.searchForm.value.key).subscribe(data=>{
        this.listb = data;
      })
  }

  onDelete(id : number){
    if(confirm("Are you sure about that bruhh ?")){
      this.svb.delete(id).subscribe(data =>{
        this.svb.getProduct().subscribe(res =>{
          this.listb = res;
        })
        this.msg = data;
        alert(this.msg.msg);
      });
    }
    
  }
  

}
