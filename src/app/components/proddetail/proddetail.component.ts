import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/models/customer';
import { Msg } from 'src/app/models/msg';
import { Product } from 'src/app/models/product';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-proddetail',
  templateUrl: './proddetail.component.html',
  styleUrls: ['./proddetail.component.css']
})
export class ProddetailComponent implements OnInit {

  id: number = 0;
  prod : Product = new Product();


  constructor(private sv: CustomerService,private svb: ProductService,private http : HttpClient,private _route : ActivatedRoute) { }
  list: Customer[] = [];
  ngOnInit(): void {
    this.sv.getCustomer().subscribe(res =>{
      this.list = res;
   })
   this.id = this._route.snapshot.params.id;
   this.svb.detail(this.id).subscribe(data =>{
     this.prod = data;
     console.log(data);
   })


  }
  
}
