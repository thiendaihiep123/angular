import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Customer } from 'src/app/models/customer';
import { Msg } from 'src/app/models/msg';
import { Product } from 'src/app/models/product';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-prodadd',
  templateUrl: './prodadd.component.html',
  styleUrls: ['./prodadd.component.css']
})
export class ProdaddComponent implements OnInit {

  constructor(private sv: CustomerService,private svb: ProductService,private http : HttpClient) { }

  prodForm:FormGroup = new FormGroup({
  
    cat: new FormControl(),
    create_at: new FormControl(),
    name: new FormControl(),
    description: new FormControl(),
    image: new FormControl(),
    price: new FormControl(),
    sale_price: new FormControl(),
    status: new FormControl()
  });

  msg : Msg = new Msg();

  list: Customer[] = [];
  ngOnInit(): void {
    this.sv.getCustomer().subscribe(res =>{
      this.list = res;
   })

  }
  onCreate(){
      this.svb.insert(this.prodForm.value).subscribe(data =>{
        this.msg = data;
        console.log(this.msg);
      });
      
  }

  

}
