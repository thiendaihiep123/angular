import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Customer } from 'src/app/models/customer';
import { Msg } from 'src/app/models/msg';
import { Product } from 'src/app/models/product';
import { CustomerService } from 'src/app/services/customer.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-prodedit',
  templateUrl: './prodedit.component.html',
  styleUrls: ['./prodedit.component.css']
})
export class ProdeditComponent implements OnInit {

  constructor(private sv: CustomerService,private svb: ProductService,private http : HttpClient,private _route : ActivatedRoute) { }

  prodFormb:FormGroup = new FormGroup({
    id: new FormControl(),
    cat: new FormControl(),
    create_at: new FormControl(),
    name: new FormControl(),
    description: new FormControl(),
    image: new FormControl(),
    price: new FormControl(),
    sale_price: new FormControl(),
    status: new FormControl()
  });

  id: number = 0;
  prod : Product = new Product();
  prodb : Product = new Product();
  idcat : number = 0;
  msg : Msg = new Msg();

  list: Customer[] = [];

  ngOnInit(): void {
    this.sv.getCustomer().subscribe(res =>{
      this.list = res;
   })

   this.id = this._route.snapshot.params.id;

   this.svb.detail(this.id).subscribe(data =>{
      this.prod = data;
      
      this.idcat = this.prod.cat.id;
      
      this.prodFormb = new FormGroup({
        id: new FormControl(data.id),
        cat: new FormControl(data.cat),
        create_at: new FormControl(data.create_at),
        name: new FormControl(data.name),
        description: new FormControl(data.description),
        image: new FormControl(data.image),
        price: new FormControl(data.price),
        sale_price: new FormControl(data.sale_price),
        status: new FormControl(data.status)
      });
     
     console.log(this.prod);
   })

  }
  
  onUpdate(){
    console.log(this.prodFormb.value);
    this.svb.update(this.prodFormb.value).subscribe(data =>{
      this.msg = data;
      console.log(this.msg);
  });
    
  }


}


